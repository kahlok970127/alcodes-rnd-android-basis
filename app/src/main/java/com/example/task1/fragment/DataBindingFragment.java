package com.example.task1.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.task1.R;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DataBindingFragment extends Fragment {

    public static final String TAG = com.example.task1.fragment.DataBindingFragment.class.getSimpleName();
    @BindView(R.id.textview_total)
    protected TextView mTextViewTotal;


    @BindView(R.id.num1)
    protected TextInputEditText editnumber1;


    @BindView(R.id.num2)
    protected TextInputEditText editnumber2;

    @BindView(R.id.num3)
    protected TextInputEditText editnumber3;

    private Unbinder mUnbinder;


    public DataBindingFragment() {
    }

    public static com.example.task1.fragment.DataBindingFragment newInstance() {
        return new com.example.task1.fragment.DataBindingFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_databinding, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
    }

    private void initView() {
        // User is logged in.
        // Show greetings message.


        editnumber1.addTextChangedListener(new TextWatcher() {


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int total = Integer.parseInt(addNumbers());

                mTextViewTotal.setText("Total =" + addNumbers());
                mTextViewTotal.setTextColor(Color.parseColor(setcolor(total)));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editnumber2.addTextChangedListener(new TextWatcher() {


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int total = Integer.parseInt(addNumbers());

                mTextViewTotal.setText("Total =" + addNumbers());
                mTextViewTotal.setTextColor(Color.parseColor(setcolor(total)));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editnumber3.addTextChangedListener(new TextWatcher() {


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                int total = Integer.parseInt(addNumbers());

                mTextViewTotal.setText("Total =" + addNumbers());
                mTextViewTotal.setTextColor(Color.parseColor(setcolor(total)));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private String setcolor(int total) {

        String colorset=null;

        if (total <= 500)
            colorset = "#a0dab3";
        else if (total <= 1000)
            colorset = "#ffff00";
        else
            colorset = "#ff0000";
        return colorset;

    }


    private String addNumbers() {
        int number1;
        int number2;
        int number3;
        if (editnumber1.getText().toString() != "" && editnumber1.getText().length() > 0) {
            number1 = Integer.parseInt(editnumber1.getText().toString());
        } else {
            number1 = 0;
        }
        if (editnumber2.getText().toString() != "" && editnumber2.getText().length() > 0) {
            number2 = Integer.parseInt(editnumber2.getText().toString());
        } else {
            number2 = 0;
        }

        if (editnumber3.getText().toString() != "" && editnumber3.getText().length() > 0) {
            number3 = Integer.parseInt(editnumber3.getText().toString());
        } else {
            number3 = 0;
        }

        return Integer.toString(number1 + number2 + number3);

    }

    @OnClick(R.id.button_reset)
    protected void startmyclick() {


        editnumber1.setText("");
        editnumber2.setText("");

        editnumber3.setText("");


    }


}
