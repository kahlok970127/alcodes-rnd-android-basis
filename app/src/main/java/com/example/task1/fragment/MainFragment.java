package com.example.task1.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.task1.Activity.DataBindingActivity;
import com.example.task1.Activity.RecycleViewActivity;
import com.example.task1.R;
import com.google.android.material.button.MaterialButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainFragment extends Fragment {

    public static final String TAG = com.example.task1.fragment.MainFragment.class.getSimpleName();

    @BindView(R.id.textview_greetings)
    protected TextView mTextViewGreetings;

    @BindView(R.id.button_disable_me)
    protected MaterialButton mButtonDisableMe;

    private Unbinder mUnbinder;
    protected static int number_of_click = 0;

    public MainFragment() {
    }

    public static com.example.task1.fragment.MainFragment newInstance() {
        return new com.example.task1.fragment.MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
    }


    @OnClick(R.id.button_click)
    protected void startmyclick() {
        number_of_click++;

        mTextViewGreetings.setText("Number Of Click  " + number_of_click);


    }

    @OnClick(R.id.button_disable_me)
    protected void disableme() {
        mButtonDisableMe.setEnabled(false);

        new CountDownTimer(6000, 10) { //Set Timer for 5 seconds
            public void onTick(long millisUntilFinished) {
                mButtonDisableMe.setText("seconds remaining: " + millisUntilFinished / 1000);

            }

            @Override
            public void onFinish() {
                mButtonDisableMe.setEnabled(true);
                mButtonDisableMe.setText("Disable Me");
            }
        }.start();

    }

    @OnClick(R.id.button_data_binding)
    protected void startDataBindingActivity() {
        startActivity(new Intent(getActivity(), DataBindingActivity.class));
    }


    @OnClick(R.id.button_recycle_view)
    protected void startViewActivity() {
        startActivity(new Intent(getActivity(), RecycleViewActivity.class));
    }


    private void initView() {
        // User is logged in.
        // Show greetings message.


        mTextViewGreetings.setText("Hello Click Me! ");

    }


}
