package com.example.task1.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.task1.Adapter.MyFriendAdapter;
import com.example.task1.Friend;
import com.example.task1.NetworkHelper;
import com.example.task1.R;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RecycleViewFragment extends Fragment {
    public static final String TAG = MainFragment.class.getSimpleName();

    private ArrayList<Friend.FriendModel> friendList = new ArrayList<>();
    @BindView(R.id.friend_recyclerview)
    protected RecyclerView mRecyclerView;


    private MyFriendAdapter mAdapter;

    private Unbinder mUnbinder;


    public RecycleViewFragment() {
    }

    public static com.example.task1.fragment.RecycleViewFragment newInstance() {
        return new com.example.task1.fragment.RecycleViewFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycleview, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        callAPI();


    }

    private void callAPI() {


        String url = "https://reqres.in/api/users?page=1";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                // TODO bad practice, should check user is still staying in this app or not after server response.

                // Convert JSON string to Java object.
                Friend responseModel = new GsonBuilder().create().fromJson(response, Friend.class);

                int i = 0;
                for (Friend.FriendModel friendModel : responseModel.data) {


                    Friend.FriendModel newfriend = new Friend.FriendModel();
                    // newfriend.id = friendModel.id;
                    //  newfriend.first_name = responseModel.data[i].first_name;
                    //  newfriend.last_name = responseModel.data[i].last_name;
                    // newfriend.avatar = responseModel.data[i].avatar;
                    // friendList.add(newfriend);
                    // i++;

                    newfriend.id = friendModel.id;
                    newfriend.first_name = friendModel.first_name;
                    newfriend.last_name = friendModel.last_name;
                    newfriend.avatar = friendModel.avatar;
                    newfriend.email = friendModel.email;
                    friendList.add(newfriend);


                }

                Log.e("d;;", "friend list: " + friendList.size());
                mAdapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                // TODO bad practice, should check user is still staying in this app or not after server response.


            }
        }) {


        };
        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
    }

    private void initView() {


        mAdapter = new MyFriendAdapter(friendList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);


    }


}
