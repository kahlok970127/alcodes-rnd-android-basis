/*
package com.example.task1.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.task1.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyFriendAdapter extends RecyclerView.Adapter<MyFriendAdapter.ViewHolder>{

    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_friend, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public Long id;
        public String image;
        public String firstName;
        public String lastName;
        public String email;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_friend_root)
        public LinearLayout root;

        @BindView(R.id.imageview_friend_image)
        public ImageView imageView;

        @BindView(R.id.textview_friend_name)
        public TextView name;

        @BindView(R.id.textview_friend_email)
        public TextView email;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            //resetViews();

            if (data != null) {
                Glide.with(root)
                        .load(data.image)
                        .into(imageView);
                name.setText(data.lastName + " " + data.firstName);
                email.setText(data.email);

                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });

                    imageView.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.showProfilePicture(data);
                        }
                    });
                }
            }
        }

        */
/*public void resetViews() {
            title.setText("");
            root.setOnClickListener(null);
            deleteButton.setOnClickListener(null);
        }*//*

    }

    public interface Callbacks {

        void onListItemClicked(DataHolder data);

        void showProfilePicture(DataHolder data);
    }
}
*/
package com.example.task1.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.task1.Friend;
import com.example.task1.R;

import java.util.List;


public class MyFriendAdapter extends RecyclerView.Adapter<MyFriendAdapter.MyViewHolder> {


    public List<Friend.FriendModel> newfriend;



    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView firstname, email;

        public MyViewHolder(View view) {
            super(view);
            image=(ImageView) view.findViewById(R.id.imageview_friend_image);
            firstname = (TextView) view.findViewById(R.id.textview_friend_name);
            email = (TextView) view.findViewById(R.id.textview_friend_email);

        }
    }


    public MyFriendAdapter(List<Friend.FriendModel> newfriend) {
        this.newfriend = newfriend;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_friend, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        //Friend.FriendModel newfriend = new Friend.FriendModel();
        Friend.FriendModel friend = newfriend.get(position);
        Glide.with(holder.image)
                .load(friend.avatar)
                .into(holder.image);
        holder.firstname.setText(friend.first_name);
        holder.email.setText(friend.email);

    }

    @Override
    public int getItemCount() {
        return newfriend.size();
    }
}