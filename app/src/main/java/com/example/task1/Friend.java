package com.example.task1;

public class Friend {
    public int page;
    public int per_page;
    public int total;
    public int total_pages;
    public FriendModel[] data;

    public static class FriendModel {

        public Long id;
        public String email;
        public String first_name;
        public String last_name;
        public String avatar;



    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public FriendModel[] getData() {
        return data;
    }

    public void setData(FriendModel[] data) {
        this.data = data;
    }
}
