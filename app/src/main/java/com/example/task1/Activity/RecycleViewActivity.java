package com.example.task1.Activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.task1.R;
import com.example.task1.fragment.RecycleViewFragment;

import butterknife.ButterKnife;

public class RecycleViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recycleview);

        ButterKnife.bind(this);

        // Check user is logged in.

        // User is logged in.
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(RecycleViewFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, RecycleViewFragment.newInstance(), RecycleViewFragment.TAG)
                    .commit();
        }

    }
}
