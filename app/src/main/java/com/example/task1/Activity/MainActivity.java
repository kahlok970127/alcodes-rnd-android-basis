package com.example.task1.Activity;


import android.os.Bundle;



import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;


import com.example.task1.fragment.MainFragment;
import com.example.task1.R;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        // Check user is logged in.

            // User is logged in.
            FragmentManager fragmentManager = getSupportFragmentManager();

            if (fragmentManager.findFragmentByTag(MainFragment.TAG) == null) {
                // Init fragment.
                fragmentManager.beginTransaction()
                        .replace(R.id.framelayout_fragment_holder, MainFragment.newInstance(), MainFragment.TAG)
                        .commit();
            }

    }
}
