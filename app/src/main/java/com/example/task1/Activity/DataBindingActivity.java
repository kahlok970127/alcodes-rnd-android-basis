package com.example.task1.Activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.task1.fragment.DataBindingFragment;
import com.example.task1.R;

import butterknife.ButterKnife;

public class DataBindingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        // Check user is logged in.

        // User is logged in.
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(DataBindingFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, DataBindingFragment.newInstance(), DataBindingFragment.TAG)
                    .commit();
        }

    }
}
